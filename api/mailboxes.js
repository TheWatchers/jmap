const fetch = require(`node-fetch`)
const auth = require(`@qnzl/auth`)

const { CLAIMS } = auth

const hostname = process.env.JMAP_HOSTNAME || "betajmap.fastmail.com"
const username = process.env.JMAP_USERNAME
const password = process.env.JMAP_PASSWORD

const authUrl = `https://${hostname}/.well-known/jmap`
const authToken = Buffer.from(`${username}:${password}`).toString(`base64`)

module.exports = async (req, res) => {
  const {
    authorization
  } = req.headers

  const isTokenValid = auth.checkJWT(authorization, CLAIMS.email.get.mailboxes, `watchers`, process.env.ISSUER)

  if (!isTokenValid) {
    return res.status(401).send()
  }

  const sessionRes = await fetch(authUrl, {
    headers: {
      'Content-Type': `application/json`,
      Authorization: `Basic ${authToken}`
    }
  })

  const body = await sessionRes.json()

  const {
    primaryAccounts,
    apiUrl
  } = body

  const accountId = primaryAccounts[`urn:ietf:params:jmap:mail`]

  const mailboxRes = await fetch(apiUrl, {
    method: `POST`,
    body: JSON.stringify({
      using: ["urn:ietf:params:jmap:core", "urn:ietf:params:jmap:mail"],
      methodCalls: [
        [
          "Mailbox/get", { accountId }, "#0"
        ]
      ]
    }),
    headers: {
      'Content-Type': `application/json`,
      Authorization: `Basic ${authToken}`
    }
  })

  const mailboxBody = await mailboxRes.json()

  const mailboxes = mailboxBody.methodResponses[0][1].list

  return res.json(mailboxes)
}


